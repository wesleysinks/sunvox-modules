# SunVox Modules

[SYNX](https://synxwtf.bandcamp.com/) SunVox modules for the incredible [SunVox modular music studio](https://warmplace.ru/soft/sunvox/). Feel free to use as desired. All modules are [licensed CC0](#license)

## List of Modules
- [SYNX_Piano](/SYNX_Piano.sunsynth)

  64 sample Steinway Model D sampled instrument.
  
  Features:
  - 2 samples per octave
  - 4 velocity layers
  - simple reverb / room fx to emulate concert hall
  - semitone transpose

- [SYNX_Swap_Stereo](/SYNX_Swap_Stereo.sunsynth)
  
  Simple stereo swapper with dry/wet control.

## Contact the Author
Email: wesley@wsle.me

## License
All modules herewithin are licensed CC0 unless specified otherwise.

CC0 1.0 Universal (CC0 1.0)
Public Domain Dedication

The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.

You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission.

This license is acceptable for Free Cultural Works.
Other Information

In no way are the patent or trademark rights of any person affected by CC0, nor are the rights that other persons may have in the work or in how the work is used, such as publicity or privacy rights.

Unless expressly stated otherwise, the person who associated a work with this deed makes no warranties about the work, and disclaims liability for all uses of the work, to the fullest extent permitted by applicable law.

When using or citing the work, you should not imply endorsement by the author or the affirmer.